﻿using System;
using System.IO;
using System.Xml.Linq;

namespace SimpleFileDirectoryViewer.Common.Extensions
{
    public static class DirectoryInfoExtension
    {

        public static long GetSize(this DirectoryInfo dirInfo, Action<Exception> exceptionHandleAction = null)
        {
            long totalSize = 0;
            if (dirInfo == null || dirInfo.Exists == false)
                return totalSize;
            using (var enumerator = dirInfo.EnumerateFiles("*.*", SearchOption.AllDirectories).GetEnumerator())
            {
                bool next = true;

                while (next)
                {
                    try
                    {
                        next = enumerator.MoveNext();
                        var file = enumerator.Current;
                        if (!next)
                            continue;
                        if (file != null)
                            totalSize += file.Length;
                    }
                    catch (Exception ex)
                    {
                        if (exceptionHandleAction != null)
                        {
                            exceptionHandleAction(ex); // for logging erros 
                        }
                    }
                }
            }
            return totalSize;
        }

        public static long GetSize(this string directoryPath, Action<Exception> exp = null)
        {
            return GetSize(new DirectoryInfo(directoryPath), exp);
        }

        public static string ConvertFileToXmlNode(this FileInfo file, Action<Exception> exceptionHandleAction = null)
        {
            try
            {
                var node = new XElement("File",
              new XElement("name", file.Name),
              new XElement("size", file.Length),
              new XElement("fullName", file.FullName),
              new XElement("creationTime", file.CreationTime),
              new XElement("lastAccessTime", file.LastAccessTime),
              new XElement("lastWriteTime", file.LastWriteTime),
              new XElement("lastModify", file.LastWriteTime),
              new XElement("attributes", file.Attributes.ToString()));
                return node.ToString();
            }
            catch (Exception ex)
            {
                if (exceptionHandleAction != null)
                {
                    exceptionHandleAction(ex); // for logging erros 
                }
                return null;
            }
        }

        public static string ConvertDirectoryToXmlNode(this DirectoryInfo directory, Action<Exception> exceptionHandleAction = null)
        {
            try
            {
                var node = new XElement("Directory",
              new XElement("name", directory.Name),
              new XElement("size", directory.GetSize(exceptionHandleAction)),
              new XElement("fullName", directory.FullName),
              new XElement("creationTime", directory.CreationTime),
              new XElement("lastAccessTime", directory.LastAccessTime),
              new XElement("lastWriteTime", directory.LastWriteTime),
              new XElement("lastModify", directory.LastWriteTime),
              new XElement("attributes", directory.Attributes.ToString()));
                return node.ToString();
            }
            catch (Exception ex)
            {
                if (exceptionHandleAction != null)
                {
                    exceptionHandleAction(ex); // for logging erros 
                }
                return null;
            }
        } 
    }
}
