﻿using System;
using System.IO;
using System.Xml.Linq;
using SimpleFileDirectoryViewer.Common.Extensions;
using SimpleFileDirectoryViewer.Common.Infrastructure;
using SimpleFileDirectoryViewer.Common.Infrastructure.EventArgs;

namespace SimpleFileDirectoryViewer.Common
{
    public class XmlSerializerWorker : IDisposable
    {
        private readonly string _pathTofile;
        private readonly DirectoryScaner _scaner;
        private readonly object _myLock = new object();
        private bool _disposed = false;

        public XmlSerializerWorker(string path, DirectoryScaner scaner)
        {
            _pathTofile = path;
            _scaner = scaner;
            DeleteFileIfExists();
        }

        public void Execute()
        {
            Initialize();
        }


        #region Subscribe methods

        private void XmlWrite_FileEvent(object sender, FileEventArgs e)
        {
            lock (_myLock)
            {
                try
                {
                    // using (XmlWriter xmlWriter = XmlWriter("Test.xml", xmlWriterSettings))
                    {
                        using (var sr = new StreamWriter(_pathTofile, true, System.Text.Encoding.UTF8))
                        {
                            sr.Write(e.Item.ConvertFileToXmlNode());
                            sr.Close();
                        }

                    }
                    if (e.Cancel)
                    {

                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        private void XmlWrite_DirectoryEvent(object sender, DirectoryEventArgs e)
        {
            lock (_myLock)
            {
                try
                {
                    using (var sr = new StreamWriter(_pathTofile, true, System.Text.Encoding.UTF8))
                    {
                        sr.Write(e.Item.ConvertDirectoryToXmlNode());
                        sr.Close();
                    }
                    if (e.Cancel)
                    {

                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        #endregion

        #region Private methods

        private void Initialize()
        {
            _scaner.FileEvent += XmlWrite_FileEvent;
            _scaner.DirectoryEvent += XmlWrite_DirectoryEvent;
        }

        private void DeleteFileIfExists()
        {
            lock (_myLock)
            {
                try
                {
                    if (File.Exists(_pathTofile))
                    {
                        File.Delete(_pathTofile);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

        }

        #endregion


        #region Dispose Methods

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_scaner != null)
                    {
                        _scaner.FileEvent -= XmlWrite_FileEvent;
                        _scaner.DirectoryEvent -= XmlWrite_DirectoryEvent;
                    }

                }
                _disposed = true;
            }
        }

        ~XmlSerializerWorker()
        {
            Dispose(false);
        }

        #endregion
    }
}