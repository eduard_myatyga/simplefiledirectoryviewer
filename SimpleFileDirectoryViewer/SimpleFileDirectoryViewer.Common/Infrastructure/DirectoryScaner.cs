﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SimpleFileDirectoryViewer.Common.Infrastructure.EventArgs;

namespace SimpleFileDirectoryViewer.Common.Infrastructure
{
    /// <summary>
    /// Defines the action on a directory
    /// </summary>
    public enum DirectoryAction
    {
        /// <summary>
        /// Enter to directory
        /// </summary>
        In,

        /// <summary>
        /// Leave from directory
        /// </summary>
        Out
    }

    public class DirectoryScaner
    {
        //for single instance of DirectoryScaner
        private static volatile DirectoryScaner _instance;
        private const string Pattern = "*.*";
        private string _rootDirectory;
        private static readonly object _eventLock = new object();
        private bool _inProgress = true;

        private EventHandler _stopedHandler;
        private FileEventHandler _fileEventHandler;
        private DirectoryEventHandler _directoryEventHandler;
        private ExeptionEventHandler _exeptionEventHandler;

        private DirectoryScaner()
        {
        }

        public static DirectoryScaner Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_eventLock)
                    {
                        if (_instance == null)
                            _instance = new DirectoryScaner();
                    }
                }

                return _instance;
            }
        }



        public void SetScanDirectory(string path)
        {
            _rootDirectory = null;
            _rootDirectory = path;
        }

        #region Delegates

        /// <summary>
        /// Definition for the FileEvent.
        ///	</summary>
        public delegate void FileEventHandler(object sender, FileEventArgs e);

        /// <summary>
        /// Definition for the DirectoryEvent.
        /// </summary>
        public delegate void DirectoryEventHandler(object sender, DirectoryEventArgs e);

        public delegate void ForceStoppingEventHandler(object sender, DirectoryEventArgs e);

        public delegate void ExeptionEventHandler(object sender, ExeptionEventArgs e);

        #endregion

        #region Events

        /// <summary>
        /// Event is raised for each file in a directory.
        /// </summary>
        public event FileEventHandler FileEvent
        {
            add
            {
                lock (_eventLock)
                {
                    _fileEventHandler += value;
                }
            }
            remove
            {
                lock (_eventLock)
                {
                    _fileEventHandler -= value;
                }
            }
        }

        public event EventHandler StopedEvent
        {
            add
            {
                lock (_eventLock)
                {
                    _stopedHandler += value;
                }
            }
            remove
            {
                lock (_eventLock)
                {
                    _stopedHandler -= value;
                }
            }
        }

        /// <summary>
        /// Event is raised for each directory.
        /// </summary>
        public event DirectoryEventHandler DirectoryEvent
        {
            add
            {
                lock (_eventLock)
                {
                    _directoryEventHandler += value;
                }
            }
            remove
            {
                lock (_eventLock)
                {
                    _directoryEventHandler -= value;
                }
            }
        }

        public event ExeptionEventHandler ExeptionEvent
        {
            add
            {
                lock (_eventLock)
                {
                    _exeptionEventHandler += value;
                }
            }
            remove
            {
                lock (_eventLock)
                {
                    _exeptionEventHandler -= value;
                }
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Raises the file event.
        /// </summary>
        private bool RaiseFileEvent(FileInfo fileInfo)
        {
            // Create a new argument object for the file event.
            FileEventArgs args = new FileEventArgs(fileInfo);
            FileEventHandler handler;
            lock (_eventLock)
            {
                handler = _fileEventHandler;
            }

            // Now raise the event.
            if (handler != null)
                handler(this, args);

            _inProgress = !args.Cancel;

            return _inProgress;
        }

        private void RaiseStopedEvent()
        {
            EventHandler handler;
            System.EventArgs args = new System.EventArgs();
            lock (_eventLock)
            {
                _inProgress = false;
                handler = _stopedHandler;
            }
            if (handler != null)
                handler(this, args);
        }

        private void RaiseExeptionEvent(Exception ex)
        {
            ExeptionEventHandler handler;
            var args = new ExeptionEventArgs(ex);
            lock (_eventLock)
            {
                _inProgress = false;
                handler = _exeptionEventHandler;
            }
            if (handler != null)
                handler(this, args);
        }

        /// <summary>
        /// Raises the directory event.
        /// </summary>
        /// <param name="directory"><see cref="DirectoryInfo"/> object for the current path.</param>
        /// <param name="action">The <see cref="DirectoryAction"/> action value.</param>
        /// <returns><see langword="true"/> when the scan is allowed to continue. <see langword="false"/> if otherwise;</returns>
        private bool RaiseDirectoryEvent(DirectoryInfo directory, DirectoryAction action)
        {
            // Only do something when the event has been declared.
            if (_fileEventHandler != null)
            {
                // Create a new argument object for the file event.
                DirectoryEventArgs args = new DirectoryEventArgs(directory, action);
                DirectoryEventHandler handler;
                lock (_eventLock)
                {
                    handler = _directoryEventHandler;
                }

                // Now raise the event.
                if (handler != null)
                    handler(this, args);

                _inProgress = !args.Cancel;
            }
            return _inProgress;
        }

        /// <summary>
        /// Walks the directory tree starting at the specified directory.
        /// </summary>
        /// <returns><see langword="true"/> when the scan is allowed to continue. <see langword="false"/> if otherwise;</returns>
        private bool WalkDirectories(DirectoryInfo directory)
        {
            // bool continueScan = inProgress;
            try
            {
                if (ProcessDirectory(directory, DirectoryAction.In))
                {
                    // Only scan the files in this path when a file event was specified 
                    if (_fileEventHandler != null)
                    {
                        _inProgress = WalkFilesInDirectory(directory);
                    }

                    if (_inProgress)
                    {
                        var subDirectories = directory.EnumerateDirectories();

                        foreach (DirectoryInfo subDirectory in subDirectories)
                        {
                            try
                            {
                                if ((subDirectory.Attributes & FileAttributes.ReparsePoint) != 0)
                                {
                                    continue;
                                }

                                if (!(_inProgress = WalkDirectory(subDirectory)))
                                {
                                    RaiseStopedEvent();
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                RaiseExeptionEvent(ex);
                            }

                        }
                    }
                    else
                    {
                        RaiseStopedEvent();
                        return false;
                    }

                    if (_inProgress)
                    {
                        _inProgress = ProcessDirectory(directory, DirectoryAction.Out);
                    }
                }
            }
            catch (Exception ex)
            {
                RaiseExeptionEvent(ex);
            }

            return _inProgress;
        }


        private bool WalkFilesInDirectory(DirectoryInfo directory)
        {

            // Scan all files in the current path
            foreach (FileInfo file in directory.EnumerateFiles(Pattern))
            {
                try
                {
                    if (_inProgress == false)
                        break;
                    if (!(_inProgress = ProcessFile(file)))
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                   RaiseExeptionEvent(ex);
                }
                
            }
            return _inProgress;
        }
        #endregion


        #region Public methods

        public void Start()
        {
            WalkDirectory();
            _inProgress = false;
            RaiseStopedEvent();
        }
        private bool WalkDirectory()
        {
            // Validate path argument.
            if (string.IsNullOrEmpty(_rootDirectory)) throw new NullReferenceException("_rootDirectory");

            return WalkDirectory(new DirectoryInfo(_rootDirectory));
        }

        /// <summary>
        /// Walks the specified directory.
        /// </summary>
        /// <param name="directory"><see cref="DirectoryInfo"/> object for the current path.</param>
        /// <returns><see langword="true"/> when the scan finished without being interupted. <see langword="false"/> if otherwise;</returns>
        private bool WalkDirectory(DirectoryInfo directory)
        {
            if (directory == null)
            {
                throw new ArgumentNullException("directory");
            }
            if (directory.Exists == false)
                throw new DirectoryNotFoundException(string.Format("Directory {0} not found", directory.Name));

            return WalkDirectories(directory);
        }

        #endregion

        #region Overridable methods

        /// <summary>
        /// Processes the directory.
        /// </summary>
        /// <returns><see langword="true"/> when the scan is allowed to continue. <see langword="false"/> if otherwise;</returns>
        public virtual bool ProcessDirectory(DirectoryInfo directoryInfo, DirectoryAction action)
        {
            DirectoryEventHandler handler;
            lock (_eventLock)
            {
                handler = _directoryEventHandler;
            }
            if (handler != null)
            {
                return RaiseDirectoryEvent(directoryInfo, action);
            }
            return true;
        }

        /// <summary>
        /// Processes the file.
        /// </summary>
        /// <returns><see langword="true"/> when the scan is allowed to continue. <see langword="false"/> if otherwise;</returns>
        public virtual bool ProcessFile(FileInfo fileInfo)
        {
            FileEventHandler handler;
            lock (_eventLock)
            {
                handler = _fileEventHandler;
            }
            if (handler != null)
            {
                RaiseFileEvent(fileInfo);
            }
            return true;
        }

        #endregion


    }
}
