﻿using System;
using System.IO;

namespace SimpleFileDirectoryViewer.Common.Infrastructure.EventArgs
{
    public class DirectoryEventArgs : BaseEventArgs<DirectoryInfo>
    {
        #region private members

        private readonly DirectoryInfo _directoryInfo;
        private readonly DirectoryAction _action;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the current directory information.
        /// </summary>
        /// <value>The <see cref="DirectoryInfo"/> object for the current directory.</value>
        public override DirectoryInfo Item
        {
            get { return _directoryInfo; }
        }

        /// <summary>
        /// Gets the current directory action.
        /// </summary>
        /// <value>The <see cref="DirectoryAction"/> action value.</value>
        public DirectoryAction Action
        {
            get { return _action; }
        }

        public override bool Cancel { get; set; }

        #endregion

        public DirectoryEventArgs(DirectoryInfo directory, DirectoryAction action)
            : base(directory)
        {
            if (directory == null) throw new ArgumentNullException("directory");
            _directoryInfo = directory;
            _action = action;
        }
    }
}
