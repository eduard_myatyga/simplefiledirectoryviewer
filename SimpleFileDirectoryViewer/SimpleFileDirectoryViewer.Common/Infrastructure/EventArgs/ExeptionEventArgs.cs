﻿using System;

namespace SimpleFileDirectoryViewer.Common.Infrastructure.EventArgs
{
    public class ExeptionEventArgs : BaseEventArgs<Exception>
    {
        public ExeptionEventArgs(Exception item) : base(item)
        {
        }
    }
}
