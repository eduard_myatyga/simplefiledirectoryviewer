﻿namespace SimpleFileDirectoryViewer.Common.Infrastructure.EventArgs
{
    public class BaseEventArgs<T> : System.EventArgs
    {
        private readonly T _item;


        /// <summary>
        /// Initializes a new instance
        /// </summary>
        /// <param name="item"><see cref="T"/> object for the current file.</param>
        protected BaseEventArgs(T item)
        {
            _item = item;
        }

        public virtual T Item { get { return _item; } }
        /// <summary>
        /// Gets or sets a value to cancel the directory scan.
        /// </summary>
        /// <value>
        /// <see langword="true"/> if the scan must be cancelled; otherwise, <see langword="false"/>.
        /// </value>
        public virtual bool Cancel { get; set; }
    }
}
