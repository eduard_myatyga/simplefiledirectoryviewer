﻿using System.IO;

namespace SimpleFileDirectoryViewer.Common.Infrastructure.EventArgs
{
    public class FileEventArgs : BaseEventArgs<FileInfo>
    {
        public FileEventArgs(FileInfo item)
            : base(item)
        {
        }
    }
}
