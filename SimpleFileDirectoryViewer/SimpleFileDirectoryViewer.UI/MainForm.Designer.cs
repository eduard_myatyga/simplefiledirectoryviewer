﻿using System.ComponentModel;

namespace SimpleFileDirectoryViewer.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pathPanel = new System.Windows.Forms.Panel();
            this.stopBtn = new System.Windows.Forms.Button();
            this.startBtn = new System.Windows.Forms.Button();
            this.outputFolderDialogBtn = new System.Windows.Forms.Button();
            this.outputFolderDialogTextBox = new System.Windows.Forms.TextBox();
            this.inputFolderDialogBtn = new System.Windows.Forms.Button();
            this.inputFolderDialogTextBox = new System.Windows.Forms.TextBox();
            this.treeViewPanel = new System.Windows.Forms.Panel();
            this.treeViewGroupBox = new System.Windows.Forms.GroupBox();
            this.treeView = new System.Windows.Forms.TreeView();
            this.inputFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.outputFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.errorsView = new System.Windows.Forms.ListBox();
            this.pathPanel.SuspendLayout();
            this.treeViewPanel.SuspendLayout();
            this.treeViewGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pathPanel
            // 
            this.pathPanel.Controls.Add(this.stopBtn);
            this.pathPanel.Controls.Add(this.startBtn);
            this.pathPanel.Controls.Add(this.outputFolderDialogBtn);
            this.pathPanel.Controls.Add(this.outputFolderDialogTextBox);
            this.pathPanel.Controls.Add(this.inputFolderDialogBtn);
            this.pathPanel.Controls.Add(this.inputFolderDialogTextBox);
            this.pathPanel.Location = new System.Drawing.Point(12, 12);
            this.pathPanel.Name = "pathPanel";
            this.pathPanel.Size = new System.Drawing.Size(660, 162);
            this.pathPanel.TabIndex = 0;
            // 
            // stopBtn
            // 
            this.stopBtn.Location = new System.Drawing.Point(232, 86);
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(221, 55);
            this.stopBtn.TabIndex = 0;
            this.stopBtn.Text = "Stop";
            this.stopBtn.UseVisualStyleBackColor = true;
            this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // startBtn
            // 
            this.startBtn.Location = new System.Drawing.Point(3, 86);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(223, 55);
            this.startBtn.TabIndex = 1;
            this.startBtn.Text = "Start";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // outputFolderDialogBtn
            // 
            this.outputFolderDialogBtn.Location = new System.Drawing.Point(493, 58);
            this.outputFolderDialogBtn.Name = "outputFolderDialogBtn";
            this.outputFolderDialogBtn.Size = new System.Drawing.Size(158, 23);
            this.outputFolderDialogBtn.TabIndex = 3;
            this.outputFolderDialogBtn.Text = "Choose output path";
            this.outputFolderDialogBtn.UseVisualStyleBackColor = true;
            this.outputFolderDialogBtn.Click += new System.EventHandler(this.outputFolderDialogBtn_Click);
            // 
            // outputFolderDialogTextBox
            // 
            this.outputFolderDialogTextBox.Location = new System.Drawing.Point(4, 60);
            this.outputFolderDialogTextBox.Name = "outputFolderDialogTextBox";
            this.outputFolderDialogTextBox.Size = new System.Drawing.Size(449, 20);
            this.outputFolderDialogTextBox.TabIndex = 2;
            // 
            // inputFolderDialogBtn
            // 
            this.inputFolderDialogBtn.Location = new System.Drawing.Point(493, 17);
            this.inputFolderDialogBtn.Name = "inputFolderDialogBtn";
            this.inputFolderDialogBtn.Size = new System.Drawing.Size(158, 23);
            this.inputFolderDialogBtn.TabIndex = 1;
            this.inputFolderDialogBtn.Text = "Choose path for scan";
            this.inputFolderDialogBtn.UseVisualStyleBackColor = true;
            this.inputFolderDialogBtn.Click += new System.EventHandler(this.inputFolderDialogBtn_Click);
            // 
            // inputFolderDialogTextBox
            // 
            this.inputFolderDialogTextBox.Location = new System.Drawing.Point(3, 19);
            this.inputFolderDialogTextBox.Name = "inputFolderDialogTextBox";
            this.inputFolderDialogTextBox.Size = new System.Drawing.Size(450, 20);
            this.inputFolderDialogTextBox.TabIndex = 0;
            // 
            // treeViewPanel
            // 
            this.treeViewPanel.Controls.Add(this.treeViewGroupBox);
            this.treeViewPanel.Location = new System.Drawing.Point(13, 180);
            this.treeViewPanel.Name = "treeViewPanel";
            this.treeViewPanel.Size = new System.Drawing.Size(659, 469);
            this.treeViewPanel.TabIndex = 1;
            // 
            // treeViewGroupBox
            // 
            this.treeViewGroupBox.Controls.Add(this.groupBox1);
            this.treeViewGroupBox.Controls.Add(this.treeView);
            this.treeViewGroupBox.Location = new System.Drawing.Point(3, 13);
            this.treeViewGroupBox.Name = "treeViewGroupBox";
            this.treeViewGroupBox.Size = new System.Drawing.Size(653, 441);
            this.treeViewGroupBox.TabIndex = 6;
            this.treeViewGroupBox.TabStop = false;
            this.treeViewGroupBox.Text = "Tree View";
            // 
            // treeView
            // 
            this.treeView.Location = new System.Drawing.Point(0, 19);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(647, 301);
            this.treeView.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.errorsView);
            this.groupBox1.Location = new System.Drawing.Point(6, 353);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(641, 88);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Error List";
            // 
            // errorsView
            // 
            this.errorsView.FormattingEnabled = true;
            this.errorsView.Location = new System.Drawing.Point(0, 19);
            this.errorsView.Name = "errorsView";
            this.errorsView.Size = new System.Drawing.Size(635, 56);
            this.errorsView.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 661);
            this.Controls.Add(this.treeViewPanel);
            this.Controls.Add(this.pathPanel);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(700, 700);
            this.MinimumSize = new System.Drawing.Size(700, 700);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.pathPanel.ResumeLayout(false);
            this.pathPanel.PerformLayout();
            this.treeViewPanel.ResumeLayout(false);
            this.treeViewGroupBox.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        

        #endregion

        private System.Windows.Forms.Panel pathPanel;
        private System.Windows.Forms.Panel treeViewPanel;
        private System.Windows.Forms.FolderBrowserDialog inputFolderDialog;
        private System.Windows.Forms.Button outputFolderDialogBtn;
        private System.Windows.Forms.Button inputFolderDialogBtn;
        private System.Windows.Forms.FolderBrowserDialog outputFolderDialog;
        private System.Windows.Forms.Button stopBtn;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.TextBox outputFolderDialogTextBox;
        private System.Windows.Forms.TextBox inputFolderDialogTextBox;
        private System.Windows.Forms.GroupBox treeViewGroupBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox errorsView;
    }
}

