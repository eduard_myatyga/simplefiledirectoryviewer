﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using SimpleFileDirectoryViewer.Common;
using SimpleFileDirectoryViewer.Common.Infrastructure;
using SimpleFileDirectoryViewer.Common.Infrastructure.EventArgs;

namespace SimpleFileDirectoryViewer.UI
{
    public partial class MainForm : Form
    {
        #region delegates

        private delegate void EnableButtonsHandler(bool enabled);

        // private MethodInvoker _showErrorAction;
        private MethodInvoker _updateInputDirectoryTextBoxAction;
        private MethodInvoker _updateOutputDirectoryTextBoxAction;
        private EnableButtonsHandler _setEnableButtonsAction;

        #endregion

        readonly DirectoryScaner _scanDirectory = DirectoryScaner.Instance;
        private Thread _scanThread = null;
        private Thread _xmlWorkerThread = null;
        private XmlSerializerWorker xmlWorker = null;
        private TreeNode _currentNode;
        private static string _outputDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "output.xml");
        private static string _inputDirectory = null;


        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            InitializeUiHandlers();
            outputFolderDialogTextBox.Invoke(_updateOutputDirectoryTextBoxAction);
        }

        private void inputFolderDialogBtn_Click(object sender, EventArgs e)
        {
            if (inputFolderDialog.ShowDialog() == DialogResult.OK)
            {
                if (Directory.Exists(inputFolderDialog.SelectedPath))
                    _inputDirectory = inputFolderDialog.SelectedPath;
                else
                {
                    MessageBox.Show(@"Directory not found", "");
                }
            }
            inputFolderDialogTextBox.Invoke(_updateInputDirectoryTextBoxAction);
        }

        private void outputFolderDialogBtn_Click(object sender, EventArgs e)
        {
            if (outputFolderDialog.ShowDialog() == DialogResult.OK)
            {
                _outputDirectory = outputFolderDialog.SelectedPath;
            }
            outputFolderDialogTextBox.Invoke(_updateOutputDirectoryTextBoxAction);
        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            
            if (Directory.Exists(_inputDirectory) == false)
            {
                MessageBox.Show(@"Directory not found", "");
                return;
            }

            _setEnableButtonsAction(false);
            try
            {
                // prepare controls / buttons for scan 
                stopBtn.Enabled = true;
                startBtn.Enabled = false;
                _currentNode = null;
                treeView.Nodes.Clear();
                

                //configuration for scan 
                _scanDirectory.SetScanDirectory(_inputDirectory);
                _scanDirectory.DirectoryEvent += treeView_DirectoryEvent;
                _scanDirectory.FileEvent += treeView_FileEvent;
                _scanDirectory.StopedEvent += ScanDirectoryOnStopedEvent;
                _scanDirectory.ExeptionEvent += ScanDirectoryOnExeptionEvent;

                //start thread scan
                _scanThread = new Thread(_scanDirectory.Start);
                _scanThread.Start();

                //start write to xml 
                _xmlWorkerThread = new Thread(delegate()
                    {
                        xmlWorker = new XmlSerializerWorker(_outputDirectory, DirectoryScaner.Instance);
                        xmlWorker.Execute();                           
                    }
                );
                _xmlWorkerThread.Start();
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                errorsView.Items.Add(ex.Message);
            }

        }

        void ScanDirectoryOnExeptionEvent(object sender, ExeptionEventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<object, ExeptionEventArgs>(delegate
                {
                    errorsView.Items.Add(e.Item.Message);

                }), sender, e);
            }
        }

        private void ScanDirectoryOnStopedEvent(object sender, EventArgs eventArgs)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<object, EventArgs>(delegate
                {
                    _inputDirectory = null;
                    _updateInputDirectoryTextBoxAction();
                    stopBtn.Enabled = false;
                    StopScan();

                }), sender, eventArgs);
            }
            
        }

        #region Tree View Subscribers

        private void treeView_FileEvent(object sender, FileEventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<object, FileEventArgs>(delegate
                {
                    if (_currentNode == null)
                        _currentNode = new TreeNode();
                    _currentNode.Nodes.Add(new TreeNode(e.Item.Name));

                    Application.DoEvents();
                    e.Cancel = !stopBtn.Enabled;

                }), sender, e);
            }

        }

        private void treeView_DirectoryEvent(object sender, DirectoryEventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<object, DirectoryEventArgs>(delegate
                {
                    switch (e.Action)
                    {
                        case DirectoryAction.In:
                            TreeNode newNode = new TreeNode(e.Item.Name);

                            if (_currentNode == null)
                            {
                                treeView.Nodes.Add(newNode);
                            }
                            else
                            {
                                _currentNode.Nodes.Add(newNode);
                            }
                            _currentNode = newNode;
                            break;

                        case DirectoryAction.Out:
                            if (_currentNode.Parent != null)
                            {
                                _currentNode = _currentNode.Parent;
                            }
                            break;
                    }

                    Application.DoEvents();

                    e.Cancel = !stopBtn.Enabled;
                }), sender, e);

            }


        }

        #endregion


        private void stopBtn_Click(object sender, EventArgs e)
        {
            stopBtn.Enabled = false;
            _currentNode = null;
            StopScan();
        }

        private void Form_Closing(object sender, CancelEventArgs e)
        {
            // Determine 
            if (_scanThread != null)
            {
                // Display a MsgBox asking the user to save changes or abort.
                if (MessageBox.Show(@"Do you want to exit?", @"",
                   MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    StopScan();
                    
                    Application.Exit();
                } 
            }
            
        }

        #region Update UI helpers

        private void InitializeUiHandlers()
        {
            _updateOutputDirectoryTextBoxAction = delegate { outputFolderDialogTextBox.Text = _outputDirectory; };
            _updateInputDirectoryTextBoxAction = delegate { inputFolderDialogTextBox.Text = _inputDirectory; };
            _setEnableButtonsAction = SetEnableButtonsForStartAction;
        }

        private void SetEnableButtonsForStartAction(bool enabled)
        {
            startBtn.Enabled = enabled;
            inputFolderDialogBtn.Enabled = enabled;
            outputFolderDialogBtn.Enabled = enabled;
        }

        private void StopScan()
        {
            try
            {
                _setEnableButtonsAction(true);
                if (_xmlWorkerThread != null)
                {
                    _xmlWorkerThread.Abort();
                    _xmlWorkerThread = null;
                }

                if (_scanThread != null)
                {
                    _scanThread.Abort();
                    _scanThread = null;
                }
                if (xmlWorker != null)
                {
                    xmlWorker.Dispose();
                }
                if (_scanDirectory != null)
                {
                    _scanDirectory.DirectoryEvent -= treeView_DirectoryEvent;
                    _scanDirectory.FileEvent -= treeView_FileEvent;
                    _scanDirectory.StopedEvent -= ScanDirectoryOnStopedEvent;
                    _scanDirectory.ExeptionEvent -= ScanDirectoryOnExeptionEvent;
                }
                
            }
            catch (Exception ex)
            {
                errorsView.Items.Add(ex.Message);
            }
        }
        #endregion
    }
}
